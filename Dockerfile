# Using node alpine as base image becasue of the size
FROM node:8-alpine

LABEL maintainer="Code Pharmacy <codepharmacy@gmail.com>"

# Creating Environment variables for source directory and angular version
ENV SRC_DIR src
ENV ANGULAR_VERSION 1.3.2

# 1. python-dev as it is needed by angular cli
# 2. ssh for git operations
# 3. git to clone remote repositries
RUN apk add --no-cache \
	python-dev \
	openssh \
	git 

# installing latest version of yarn
RUN npm install -g yarn

# installing angular cli as global package
RUN yarn global add \
	@angular/cli@$ANGULAR_VERSION \
	pm2 \
	&& pm2 install typescript

# setting yarn as default package for ng as it is fast, secure and developer friendly
RUN ng set --global packageManager=yarn

# Creating a source directory in order to avoid putting project source code at root level 
RUN mkdir $SRC_DIR

# Switching to source directory
WORKDIR $SRC_DIR

# Running ash to be able to run the commands as soon as the container is up
CMD ["/bin/ash"]