ssh-keyscan $GIT_REPO_TYPE >> $SSH_DIR/known_hosts

git clone git@$GIT_REPO_TYPE:$GIT_REPO_OWNER_NAME/$GIT_REPO_NAME.git 

cd $GIT_REPO_NAME

git checkout -b $GIT_BRANCH_NAME origin/$GIT_BRANCH_NAME

yarn

npm run prestart

pm2 start src/server.ts

/bin/ash